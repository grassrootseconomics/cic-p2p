// this site broke the ice (no pun intended) for me on the topic of signaling. thanks!
// https://www.tutorialspoint.com/webrtc/webrtc_signaling.htm

const WebSocketServer = require('ws').Server;
const uuid = require('uuid');
 
let room = {};

const wss = new WebSocketServer({port: 9090}); 

async function sendOffers(p, offers) {
	offers.forEach((offer) => {
		const candidate_announce = {
			id: p.remote_id,
			offer: offer,
		}
		const j = JSON.stringify(candidate_announce);
		console.log('sending back', p.remote_id, j);
		p.conn.send(j);
	});
}

async function tattle(source_id, offers) {
	Object.keys(room).forEach((k) => {
		const p = room[k];
		console.log('room', k);
		if (source_id == p.remote_id) {
			return;
		}
		sendOffers(p, offers);
	});
}

function validateId(id) {
	if (id === undefined || id == '') {
		throw 'empty id';
	}

}

function Peer(peer_id, conn, req) {
	let self = this;

	self.remote_id = peer_id;
	self.offers = {};
	self.answers = [];
	self.candidates = [];
	self.conn = conn;

	conn.on('message', (raw_data) => {
		let data;
		
		try {
			data = JSON.parse(raw_data);
			validateId(data.peer_id);
		} catch(e) {
			console.warn('invalid request: ' + e);
			return;
		};
		if (room[data.peer_id] === undefined) {
			console.info('unknown peer: ' + data.peer_id);
			return;
		}
		if (data.peer_id == self.remote_id) {
			console.warn('peer is self: ' + data.peer_id);
			return;
		}

		// valid message, handle cases
		if (data.detail.type == 'offer') {
			self.offers[data.peer_id] = data.detail;
			const p = room[data.peer_id];
			data.peer_id = self.remote_id;
			const j = JSON.stringify(data);
			console.debug('offer', self.remote_id, data.peer_id, p.remote_id);
			p.conn.send(j);
		} else if (data.detail.type == 'answer') {
			self.offers[data.peer_id] = data.detail;
			const p = room[data.peer_id];
			const candidate_peer_id = data.peer_id;
			data.peer_id = self.remote_id;
			let j = JSON.stringify(data);
			console.debug('answer', self.remote_id, data.peer_id, p.remote_id);
			p.conn.send(j);
			p.candidates.forEach((c) => {
				const o = {
					peer_id: candidate_peer_id,
					detail: c,
				}
				j = JSON.stringify(o);
				self.conn.send(j);
			});
		} else if (data.detail.candidate !== undefined) {
			self.candidates.push(data.detail.candidate);
			console.debug('candidate', self.remote_id, data.detail.candidate);
		}
	});

	conn.on('close', (e, v) => {
		const clientKey = room[self.remote_id];
		if (clientKey !== undefined) {
			delete room[self.remote_id]; 
		}
		console.log('deleted record', self.remote_id);
	});
}

function urlToId(u) {
	return u.substring(1);
}

wss.on('connection', (conn, req) => {
	const peer_id = urlToId(req.url);
	const p = new Peer(peer_id, conn, req);
	console.log('new connection', peer_id);

	const new_peer_msg = {
		peers: [peer_id],
	}
	let j = JSON.stringify(new_peer_msg);

	let peers = [];
	Object.keys(room).forEach((k) => {
		if (k == peer_id) {
			return;
		}
		peers.push(k);
		const p = room[k];
		console.debug('sending new peer', k, j);
		p.conn.send(j);
	});
	const peers_msg = {
		peers: peers,
	}
	j = JSON.stringify(peers_msg);
	console.debug('sending peers', peer_id, j);
	p.conn.send(j);
	
	room[peer_id] = p;
});
