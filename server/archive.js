const SimplePeer = require('simple-peer');
const WebSocket = require('ws');
const wrtc = require('wrtc');

const id = 'guru';

const ws = new WebSocket('ws://localhost:9090/' + id);

let peers = {};

ws.onopen = function(e) {
	console.log(e);

};

ws.onmessage = function(m) {
	let peer = undefined;
	const o = JSON.parse(m.data);
	if (o.peers !== undefined) {
		console.debug('peersmsg', o.peers);
		o.peers.forEach((peer_id) => {
			if (peers[peer_id] === undefined) {
				console.log('new peer', peer_id);
				const peer = new SimplePeer({
					initiator: true,
					trickle: false,
					channelName: id,
					config: {
						iceServers: [
//								{
//									urls: 'stun:holbrook.no:3478',
//								},
							],
					},
					wrtc: wrtc,
				});
				peer.on('signal', (data) => {
					console.log('signaling', data);
					const o = {
						peer_id: peer_id,
						detail: data,
					}
					const j = JSON.stringify(o)
					ws.send(j);
				});

				peer.on('connect', () => {
					console.log('connect');
				});

				peers[peer_id] = {
					peer: peer,
					answered: false,
					}
				console.log('setting peers', peer_id);
			}
		});
		newbie = false;
	} else if (o.detail.type == 'offer') {
		console.log('getting peers', o.peer_id);
		const peer = peers[o.peer_id].peer;
		console.log('offer', o.peer_id, o.detail, peer);
		if (!peer.answered) {
			peer.signal(o.detail);
		}
	} else if (o.detail.type == 'answer') {
		console.log('answer', o.peer_id, o.detail);
		const peer = peers[o.peer_id].peer;
		peer.signal(o.detail);
	}
}
